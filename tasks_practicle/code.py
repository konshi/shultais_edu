def code(text, key=1):
    out = ''
    for ch in text:
        out += chr(ord(ch) + key)
    return out
