def choose_plural(count, word_forms):
    list_digit = list(str(count))
    split_list = word_forms.split(',')
    last = list_digit[-1]
    pre_last = list_digit[-2] if len(list_digit) > 1 else None
    if pre_last in ['1']:
        return split_list[2]
    elif last in ['1']:
        return split_list[0]
    elif last in ['2', '3', '4']:
        return split_list[1]
    else:
        return split_list[2]
