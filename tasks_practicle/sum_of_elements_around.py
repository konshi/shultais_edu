import sys
row = int(sys.argv[1])
column = int(sys.argv[2])
# импортируем данные матрицы из файла и разбиваем по строкам
with open('data.txt', 'r') as file_data:
    rows_list = file_data.read().strip().split('\n')
# формируем строки матрицы в виде отдельного списка
rows = []
for r in rows_list:
    r = list(r.split())
    rows.append(r)
# формируем колонки матрицы в виде отдельного списка
columns = []
temp_columns = []
c = 0
c1 = 0
while c1 < len(rows[0]):
    if c == len(rows):
        columns.extend([temp_columns])
        temp_columns = []
        c1 += 1
        c = 0
    else:
        temp_columns.append(rows[c][c1])
        c += 1
# координаты переданного числа и его строки
current_row = rows[row]
current_number = rows[row][column]
# координаты элементов над числом
up_row_current_n = columns[column][row - 1] if row > 0 else 0
up_row_prev_n = columns[column - 1][row - 1] if row > 0 and column > 0 else 0
up_row_next_n = rows[row - 1][column + 1] if row > 0 and column < len(rows[row]) - 1 else 0
# координаты элементов под числом
down_row_current_n = rows[row + 1][column] if row < len(rows) - 1 else 0
down_row_prev_n = rows[row + 1][column - 1] if row < len(rows) - 1 and column > 0 else 0
down_row_next_n = rows[row + 1][column + 1] if row < len(rows) - 1 and column < len(rows[row]) - 1 else 0

# координаты чисел ДО и ПОСЛЕ переданного числа в текущей строке
prev_n = rows[row][column - 1] if column > 0 else 0
next_n = rows[row][column + 1] if column < len(rows[row]) - 1 else 0
# собираем итоговый список элементов вокруг числа и преобразуем элементы из строкового типа в числовой
list_num = up_row_prev_n, up_row_current_n, up_row_next_n, prev_n, next_n, down_row_prev_n, down_row_current_n, down_row_next_n
int_list_sum = []
for l in list_num:
    int_list_sum.append(int(l))
# суммируем финальный результат
int_list_sum = sum(int_list_sum)
print(int_list_sum)
