import sys
input_sentence = sys.argv[1]

punctuation_list = [',', ' ', '!', '?', '.']
out_words = []
temp = ''
for char in input_sentence:
    if char not in punctuation_list:
        temp += char
    elif char in punctuation_list and len(temp) > 0:
        out_words.append(temp)
        temp = ''
out_words.append(temp) if len(temp) > 0 else None

print(len(out_words))

# python count.py 'Я     пошел  в ТЦ    Европа    в магазин Варежки .'
# > 8