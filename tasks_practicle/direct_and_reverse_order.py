import sys

some_string = sys.argv[1]

revers_string = some_string.split(' ')
end_str = ''
for char in revers_string:
    char = char[::-1] + ' '
    end_str += char
end_str.strip()

print(end_str)

# > python reverse.py "яблоки вкусные"
# иколбя еынсукв