import sys
input_date = sys.argv[1]
# need convert '01.01.2016' to '01/Jan/2016'
convert_date = input_date
with open('server.log') as logs_file:
    search_list = []
    while True:
        line = logs_file.readline()
        if '01/Jan/2016' in line:
            search_list.append(line.split()[3])
        elif line == '':
            break
    result_list = list(set(search_list))
    out_calc = []
    for r in result_list:
        out_calc.append([r, search_list.count(r)])
    sorted_list = max(out_calc, key=lambda x: int(x[1]))
    print(search_list, result_list, sorted_list, sep='\n')


# Пример работы программы:
# Программа выводит, что 01.01.2016 страницу / посетили 795 раз.
# > python analyze.py 01.01.2016
# > / 795
