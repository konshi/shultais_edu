from datetime import datetime, timedelta


def dates(date ):
    f_dt = datetime.strptime(date, "%d.%m.%Y").date()
    pre_day = f_dt - timedelta(days=1)
    nex_day = f_dt + timedelta(days=1)
    res_out = ('{:%d.%m.%Y}'.format(pre_day), '{:%d.%m.%Y}'.format(nex_day))
    return (res_out)
