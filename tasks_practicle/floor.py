def floor(ap_num, fl_count):
    return (ap_num / fl_count).__ceil__()

# print(floor(25, 5))
# result = floor(8, 3)
# print(result)
# 3
# >>> floor(1, 3)
# 1

