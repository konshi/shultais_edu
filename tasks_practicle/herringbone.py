import sys
numbers = int(sys.argv[1])

number = 1
numbers_in_line = 1

# В данной задаче используем вложенные друг в друга while циклы
while number <= numbers:
    number_in_line = 1
    line = []

    # Важно проверять как формирование очередной строки,
    # так и следить за общим количеством чисел
    while number_in_line <= numbers_in_line and number <= numbers:
        line.append(str(number))
        number_in_line += 1
        number += 1

    print(" ".join(line))

    numbers_in_line += 1

# from sys import argv
# from itertools import count, accumulate
#
#
# arg = int(argv[1])
# it = accumulate(count(1))
# breakers = []
#
#
#
# while True:
#     i = next(it)
#     breakers.append(i)
#     if i > arg:
#         break
#
#
#
# for n in range(1, arg+1):
#     end = '\n' if n in breakers else ' '
#     print(n, end=end)
# > python program.py 17
# 1
# 2 3
# 4 5 6
# 7 8 9 10
# 11 12 13 14 15
# 16 17
