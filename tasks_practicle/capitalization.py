def capitalization(money, percent, period):
    account = money
    period_list = list(range(1, period + 1))
    for p in period_list:
        profit = (account / 100 * percent) / 12
        account += profit
    return (int(account))
