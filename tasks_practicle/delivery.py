def delivery(price, region=False, big=False, time=False, today=False):
    delivery_price = 0
    if today:
        delivery_price += 990

    if price < 5000 and big:
        delivery_price += 490
    elif price < 5000:
        delivery_price += 290
    elif region and not big:
        delivery_price += 290
    elif big:
        delivery_price += 490
    elif big:
        delivery_price += 490
    return delivery_price + 190 if time else delivery_price
