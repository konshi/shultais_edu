def distance(crd1, crd2):
    d2 = ((crd2[0] - crd1[0]) ** 2 + (crd2[1] - crd1[1]) ** 2) ** 0.5 if len(crd1) == 2 else None
    d3 = ((crd2[0] - crd1[0]) ** 2 + (crd2[1] - crd1[1]) ** 2 + (crd2[2] - crd1[2]) ** 2) ** 0.5 if len(crd1) == 3 else None
    if len(crd1) == 2:
        return d2
    else:
        return d3

# AB = √(xb - xa)2 + (yb - ya)2
# print(distance((-1, 3), (6, 2)))
# 7.0710678118654755
# print(distance((-1, 3, 3), (6, 2, -2)))
# 8.660254037844387
# d3 = ((x2−x1)2+(y2−y1)2+(z2−z1)2) ** 0.5
