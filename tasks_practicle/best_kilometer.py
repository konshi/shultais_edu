# импортируем исходные данные в переменную times_form
file_times = open('run.txt', 'r')
times = file_times.read().strip().split('\n')
# создаём список измерений 200м отрезков
times_form = []
for t in times:
    t = int(t)
    times_form.append(t)
times_list = []
temp_list = []
i = 0
while i < len(times_form):
    temp_list.append(times_form[i])
    i += 1
    if len(temp_list) == 5:
        times_list.extend([temp_list])
        temp_list = []
        times_form.pop(0)
        i = 0
        if not times_form:
            times_list.extend([temp_list])
times_list = list(map(sum, times_list))
# сортируем и получаем самый быстрый километр в сек.
better_km = sorted(times_list)[0]
# преобразуем секунды в мм:сек
min = int(better_km / 60)
sec = better_km % 60
# добавляем ноль если значение меньше 10
min = min if min > 9 else f'0{min}'
sec = sec if sec > 9 else f'0{sec}'
# итоговый результат
print(min, sec, sep=':')
