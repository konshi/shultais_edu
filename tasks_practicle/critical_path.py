table_file = open('table.txt').read()
graf = table_file.strip().split(',')
table = {}
for t in graf:
    table.update({t.split(':')[0]:t.split(':')[1]})
track = ''
for k, v in table.items():
    for k1, v1 in table.items():
        if v1 == k:
            track += f'{k}, {k1}, '
            break
print(track)
# with open('table.txt') as file:
#     table = dict(map(lambda x: x.split(':'), file.read().split(',')))
# path = ['1']
# while path[-1] != '0':
#     path.append(table[path[-1]])
# print(', '.join(reversed(path)))
# table = {"0": None, "A": 0, ...}
# dictionary[key] = value
# table.update(other dict)
# > python path.py
# > 0, A, Z, B, M, 1

# {'0': '', 'A': '0', 'N': 'A', 'T': 'B', 'Z': 'A', 'B': 'Z', 'M': 'B', 'X': 'A', '1': 'M'}

# dict_keys(['0', 'A', 'N', 'T', 'Z', 'B', 'M', 'X', '1'])
# dict_values(['', '0', 'A', 'B', 'A', 'Z', 'B', 'A', 'M'])
