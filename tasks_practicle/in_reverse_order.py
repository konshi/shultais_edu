import sys

input_text = reversed(sys.argv[1])
out_text = ''
for i in input_text:
    out_text += i
print(out_text)

# решение преподавателя
# import sys
# word = sys.argv[1]
#
# # Используем срез [::-1], который переворачивает строки и списки.
# print(word[::-1])
#
# # Срез [::-1] означает, что нам нужна строка с конца и до начала.
# # -1 задает обратный порядок следования символов.