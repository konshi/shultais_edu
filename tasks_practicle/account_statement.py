accaunt_list = []

for line in open("bank.txt", "r", encoding="utf-8"):
    line = line.strip().split(";")
    outcome, income, value = line
    if outcome == '000' or income == '000':
        continue
    else:
        accaunt_list.append(outcome)
        accaunt_list.append(income)
unique_accaunt_list = list(set(accaunt_list))
formatted_list = []
for j in unique_accaunt_list:
    j = [0, j]
    formatted_list.append(j)

i = 0
while i < len(formatted_list):
    for line in open("bank.txt", "r", encoding="utf-8"):
        line = line.strip().split(";")
        outcome, income, value = line
        if formatted_list[i][1] == outcome:
            formatted_list[i][0] -= int(value)
        elif formatted_list[i][1] == income:
            formatted_list[i][0] += int(value)
    i += 1
sort_list = sorted(formatted_list)
final_output = ''
for k in sort_list:
    balance = k[0]
    num_accaunt = k[1]
    template = f'{num_accaunt} {balance};'
    final_output += template

print(final_output.strip(';'))


# > python program.py
# 789 -300;456 -100;123 600
